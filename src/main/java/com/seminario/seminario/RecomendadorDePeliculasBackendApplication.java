package com.seminario.seminario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecomendadorDePeliculasBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecomendadorDePeliculasBackendApplication.class, args);
	}

}
