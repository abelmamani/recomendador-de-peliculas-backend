package com.seminario.seminario.services;

import com.seminario.seminario.models.MovieNode;
import java.util.Collection;

public interface MovieService {
    Collection<MovieNode> getAllMovies();
    MovieNode getMovieById(Long id);
}
