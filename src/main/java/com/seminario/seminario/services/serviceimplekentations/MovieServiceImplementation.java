package com.seminario.seminario.services.serviceimplekentations;

import com.seminario.seminario.repositoresi.MovieRepository;
import com.seminario.seminario.exceptions.MovieException;
import com.seminario.seminario.models.MovieNode;
import com.seminario.seminario.services.MovieService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@AllArgsConstructor
@Service
public class MovieServiceImplementation implements MovieService {
    private MovieRepository movieRepository;
    @Override
    public Collection<MovieNode> getAllMovies() {
        return movieRepository.findAll();
    }

    @Override
    public MovieNode getMovieById(Long id) {
        Optional<MovieNode> movie = movieRepository.findById(id);
        if(movie.isEmpty())
            throw new MovieException("The movie with id " + id + "not exist");
        return movie.get();
    }
}
