package com.seminario.seminario.services.serviceimplekentations;

import com.seminario.seminario.repositoresi.GenreRepository;
import com.seminario.seminario.models.GenreNode;
import com.seminario.seminario.services.GenreService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.Collection;

@AllArgsConstructor
@Service
public class GenreServiceImplementation implements GenreService {
    private GenreRepository genreRepository;
    @Override
    public Collection<GenreNode> getAllGenres() {
        return genreRepository.findAll();
    }
}
