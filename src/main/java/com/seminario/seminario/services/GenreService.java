package com.seminario.seminario.services;

import com.seminario.seminario.models.GenreNode;
import java.util.Collection;

public interface GenreService {
    Collection<GenreNode> getAllGenres();
}
