package com.seminario.seminario.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.neo4j.core.schema.*;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Node("Movie")
public class MovieNode {
    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private String certificate;
    private Double rating;
    private Integer year;
    private Integer duration;
    private Integer votes;

    @Relationship(type = "BELONGS_TO", direction = Relationship.Direction.OUTGOING)
    private List<GenreNode> genres;
}
