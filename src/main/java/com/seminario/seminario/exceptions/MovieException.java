package com.seminario.seminario.exceptions;

public class MovieException extends RuntimeException{
    public MovieException(String msg){
        super(msg);
    }
}
