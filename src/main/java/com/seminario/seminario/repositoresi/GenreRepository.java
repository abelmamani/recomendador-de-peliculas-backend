package com.seminario.seminario.repositoresi;

import com.seminario.seminario.models.GenreNode;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenreRepository extends Neo4jRepository<GenreNode, String> {
}
