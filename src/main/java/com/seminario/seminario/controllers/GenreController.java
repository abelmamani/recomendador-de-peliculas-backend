package com.seminario.seminario.controllers;

import com.seminario.seminario.services.serviceimplekentations.GenreServiceImplementation;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/genres")
@CrossOrigin("*")
@AllArgsConstructor
public class GenreController {
    private GenreServiceImplementation genreServiceImplementation;
    @GetMapping
    public ResponseEntity<?> getAllMovies(){
        return ResponseEntity.ok(genreServiceImplementation.getAllGenres());
    }
}
