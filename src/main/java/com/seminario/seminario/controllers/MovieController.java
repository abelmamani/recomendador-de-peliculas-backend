package com.seminario.seminario.controllers;

import com.seminario.seminario.services.serviceimplekentations.MovieServiceImplementation;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/movies")
@CrossOrigin("*")
@AllArgsConstructor
public class MovieController {
    private MovieServiceImplementation movieServiceImplementation;
    @GetMapping
    public ResponseEntity<?> getAllMovies(){
        return ResponseEntity.ok(movieServiceImplementation.getAllMovies());
    }
    @GetMapping("/{id}")
    public  ResponseEntity<?> getMovieByid(@PathVariable("id") Long id){
        try {
            return ResponseEntity.ok(movieServiceImplementation.getMovieById(id));
        }catch (RuntimeException exception){
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }
}
